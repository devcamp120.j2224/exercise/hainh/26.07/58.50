package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Employee;

public interface iEmployeeRespository extends JpaRepository<Employee , Long> {
    
}
