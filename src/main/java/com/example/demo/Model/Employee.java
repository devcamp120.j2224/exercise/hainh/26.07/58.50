package com.example.demo.Model;

import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class Employee {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private long id ;

        @Column(name = "first_name")
        private String firtname ;

        @Column(name = "last_name")
        private String latname ;

        @Column(name = "email")
        private String email ;

        @Column(name = "age")
        private int age ;

        @Column(name = "update_date")
        private Date updateDate ;


        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getFirtname() {
            return firtname;
        }

        public void setFirtname(String firtname) {
            this.firtname = firtname;
        }

        public String getLatname() {
            return latname;
        }

        public void setLatname(String latname) {
            this.latname = latname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public Date getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(Date updateDate) {
            this.updateDate = updateDate;
        }

        public Employee() {
        }

        public Employee(long id, String firtname, String latname, String email, int age, Date updateDate) {
            this.id = id;
            this.firtname = firtname;
            this.latname = latname;
            this.email = email;
            this.age = age;
            this.updateDate = updateDate;
        }



}
